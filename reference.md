---
layout: reference
---

## References

### Pandas

#### Cheatsheets

* [Pandas' Official Cheatsheet](
      https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf
  )

* [Python for Data Science (with Pandas) Cheatsheet](
      https://www.datacamp.com/community/blog/python-pandas-cheat-sheet
  )

These are handy reminders to help you
write your own analysis pipeline using _pandas_.
Please study these resources and keep them within easy reach.


### Seaborn

* [Seaborn Official Tutorial](https://seaborn.pydata.org/tutorial.html)

* [Seaborn Gallery of Examples](https://seaborn.pydata.org/examples/index.html)



### Spark & PySpark

#### PySpark overview and programming guides

* [Spark version 2.3 documentation website](
      https://spark.apache.org/docs/2.3.0/
  )

* [Spark version 1.6 documentation website](
      https://spark.apache.org/docs/1.6.0/
  )

#### PySpark API reference

* [Python API for Spark version 2.3](
      https://spark.apache.org/docs/2.3.0/api/python/index.html
  )

* [Python API for Spark version 1.6](
      https://spark.apache.org/docs/1.6.0/api/python/index.html
  )

#### On RDD and DataFrame

* [A Tale of Three Apache Spark APIs: RDDs, DataFrames, and Datasets -- When to use them and why](
      https://databricks.com/blog/2016/07/14/a-tale-of-three-apache-spark-apis-rdds-dataframes-and-datasets.html
  )

Note that Dataset is a general case of DataFrame; however Dataset API is
supported only on Scala and Java.

#### Spark running modes

This is a very technical aspect of Spark, which may be needed by people who
set up their own Spark cluster.

* [Spark on YARN](https://spark.apache.org/docs/2.3.0/running-on-yarn.html):
  This is the "traditional" way of deploying Spark on a Hadoop cluster,
  coupled with HDFS as the filesystem backend.

* [Spark standalone mode](https://spark.apache.org/docs/2.3.0/spark-standalone.html):
  In this mode, Spark master and worker processes must be set up manually
  (possibly with the help of some setup scripts).

* It is also possible to run Spark with
  [Mesos](https://spark.apache.org/docs/2.3.0/running-on-mesos.html) and
  [Kubernetes](https://spark.apache.org/docs/2.3.0/running-on-kubernetes.html),
  but it is outside the scope of our training.


### Computer Notes

#### Networking

* [List of Popular TCP port numbers](http://www.meridianoutpost.com/resources/articles/well-known-tcpip-ports.php)



## Glossary

{:auto_ids}
action (Spark)
:   A method of a Spark RDD to invoke the computation and return the computed results.

attribute (object)
:   In object-oriented programming, an attribute can be thought of as a variable,
    or a value, that belongs to an object.
    For example, a DataFrame object called `df` has an attribute called `shape`
    which describes the dimensions of the tabular dataset.
    An attribute has to be retrieved along with its owning object, e.g. `df.shape`.
    An attribute should **not** be called with the function call `()` operator.

descriptive statistics
:   TODO

Resilient Distributed Dataset (Spark)
:   Resilient Distributed Dataset (RDD) is a representation of dataset in Spark that
    can be distributed across multiple machines and is resilient against network
    or computer failure.

nested list
:   TODO

network flow
:   A network flow, or a traffic flow, or a packet flow, is a sequence
    of packets from a source computer to a destination, which may be
    another host, a multicast group, or a broadcast
    domain.
    ([Wikipedia definition](https://en.wikipedia.org/wiki/Traffic_flow_(computer_networking)))

transformation (Spark)
:   A method of a Spark RDD which transforms the data into another form; it returns another RDD.

{% include links.md %}
