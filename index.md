---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---

<!--
WEBSITE:
<https://deapsecure.gitlab.io/deapsecure-lesson02-bd/index.html>
-->

<!-- this is an html comment -->

{% comment %} This is a comment in Liquid {% endcomment %}

The Big Data lesson module introduces an efficient way
of handling, processing, and analyzing large amounts of data
using **pandas**, **matplotlib** and **seaborn**.
[pandas][pandas] is the de facto data analysis and manipulation tool
for Python programming language.
[Matplotlib][matplotlib] and [Seaborn][seaborn] are
visualization packages for data analysis in Python.
The data handling skills introduced in this lesson
form the foundation for the subsequent two lessons
on [machine learning][deapsecure-ml-lesson]
and [neural networks][deapsecure-nn-lesson].


> ## Prerequisites
>
> * Learners should have acquired basic skills in Python programming
>   in order to learn this lesson effectively.
>   Learners that are new to Python are encouraged to take a
>   brief tutorial on Python, such as
>   the [Plotting and Programming in Python][python-gapminder] lesson by
>   [Software Carpentry][swc-lessons].
>   The DeapSECURE project also maintains
>   a [list of Python crash courses][DS-list-python-courses].
>
> * This lesson module requires Python, Pandas, Matplotlib and Seaborn.
>   For optimal teaching and learning experience, please use Jupyter Notebook
>   or JupyterLab.
>
{: .prereq}

{% include links.md %}
