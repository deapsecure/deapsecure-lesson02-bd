Images in this directory are from the SherLock Team,

http://bigdata.ise.bgu.ac.il/sherlock/#/team

The copyright was held by them, included here
under "fair use" term that this is for non-commercial
educational purposes.
For reuse, you should contact the authors directly
to request for permission.
