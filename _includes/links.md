[cc-by-human]: https://creativecommons.org/licenses/by/4.0/
[cc-by-legal]: https://creativecommons.org/licenses/by/4.0/legalcode
[ci]: http://communityin.org/
[coc-reporting]: https://docs.carpentries.org/topic_folders/policies/code-of-conduct.html#reporting-guidelines
[coc]: https://docs.carpentries.org/topic_folders/policies/code-of-conduct.html
[concept-maps]: https://carpentries.github.io/instructor-training/05-memory/
[contrib-covenant]: https://contributor-covenant.org/
[contributing]: {{ repo_url }}/blob/{{ source_branch }}/CONTRIBUTING.md
[cran-checkpoint]: https://cran.r-project.org/package=checkpoint
[cran-knitr]: https://cran.r-project.org/package=knitr
[cran-stringr]: https://cran.r-project.org/package=stringr
[dc-lessons]: http://www.datacarpentry.org/lessons/
[email]: mailto:team@carpentries.org
[github-importer]: https://import.github.com/
[importer]: https://github.com/new/import
[jekyll-collection]: https://jekyllrb.com/docs/collections/
[jekyll-install]: https://jekyllrb.com/docs/installation/
[jekyll-windows]: http://jekyll-windows.juthilo.com/
[jekyll]: https://jekyllrb.com/
[jupyter]: https://jupyter.org/
[lc-lessons]: https://librarycarpentry.org/#portfolio
[lesson-example]: https://carpentries.github.io/lesson-example/
[mit-license]: https://opensource.org/licenses/mit-license.html
[morea]: https://morea-framework.github.io/
[numfocus]: https://numfocus.org/
[osi]: https://opensource.org
[pandoc]: https://pandoc.org/
[paper-now]: https://github.com/PeerJ/paper-now
[python-gapminder]: https://swcarpentry.github.io/python-novice-gapminder/
[pyyaml]: https://pypi.python.org/pypi/PyYAML
[r-markdown]: https://rmarkdown.rstudio.com/
[rstudio]: https://www.rstudio.com/
[ruby-install-guide]: https://www.ruby-lang.org/en/downloads/
[ruby-installer]: https://rubyinstaller.org/
[rubygems]: https://rubygems.org/pages/download/
[styles]: https://github.com/carpentries/styles/
[swc]: https://software-carpentry.org/
[swc-lessons]: https://software-carpentry.org/lessons/
[swc-releases]: https://github.com/swcarpentry/swc-releases
[workshop-repo]: {{ site.workshop_repo }}
[yaml]: http://yaml.org/

[pandas]: https://pandas.pydata.org/
[matplotlib]: https://matplotlib.org/
[seaborn]: https://seaborn.pydata.org/

{% comment %} DeapSECURE-specific links {% endcomment %}

[deapsecure-website]: https://deapsecure.gitlab.io/
[deapsecure-hpc-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson01-hpc/
[deapsecure-bd-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson02-bd/
[deapsecure-ml-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson03-ml/
[deapsecure-nn-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson04-nn/
[deapsecure-crypt-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson05-crypt/
[deapsecure-par-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson06-par/

[DS-list-python-courses]: https://deapsecure.gitlab.io/posts/2020/01/crash-course-python/

{% comment %} SherLock resources {% endcomment %}
[sherlock-overview_orig]: http://bigdata.ise.bgu.ac.il/sherlock/#/
[sherlock-overview]: https://web.archive.org/web/20200602173700/http://bigdata.ise.bgu.ac.il/sherlock/#/
[sherlock-pub-2016]: https://dl.acm.org/doi/10.1145/2996758.2996764
{% comment %} This is now invalid as of 2023 -- Need to re-host on our drive (see the second link below) {% endcomment %}
[sherlock-desc-PDF_orig]: https://drive.google.com/file/d/0B_A1qX1kf7R9Q0llRWpkY2pXdzg/view
[sherlock-desc-PDF]: https://drive.google.com/file/d/1ZlG3iIVZQEa-tUUkfdN14merwfkAHV--/view
[sherlock-dataset-kaggle]: https://www.kaggle.com/datasets/BGU-CSRC/sherlock/data
[sherlock-Applications.csv]: https://drive.google.com/file/d/1GfLOohkjIMQeOxQb8fIcTRZlHT46_f7M/view

[BD-ep02-big-data-sherlock]: {{ page.root }}{% link _episodes/02-big-data-sherlock.md %}
[BD-ep20-analytics-intro]: {{ page.root }}{% link _episodes/20-analytics-intro.md %}
[BD-extras-sherlock]: {{ page.root }}{% link _extras/dataset-sherlock.md %}

[NN-ep24-keras-classify]: {{ site.deapsecure_nn_lesson }}/24-keras-classify/ %}

[sherlock-overview]: http://bigdata.ise.bgu.ac.il/sherlock/#/
[sherlock-inet-archive]: https://web.archive.org/web/20190829054234/http://bigdata.ise.bgu.ac.il/sherlock/#/
[sherlock-pub-2016]: https://dl.acm.org/doi/10.1145/2996758.2996764
[sherlock-pub-2016-bgu]: https://cyber.bgu.ac.il/wp-content/uploads/2017/10/p1-mirsky.pdf
[sherlock-pub-2016-gdrive]: https://drive.google.com/file/d/0B_A1qX1kf7R9OVhhVk5wNjkydkU/view?resourcekey=0-0nOQiDtPQeH1XfKa9glDHA
[sherlock-pub-2016-resgate]: https://www.researchgate.net/publication/310821462_SherLock_vs_Moriarty_A_Smartphone_Dataset_for_Cybersecurity_Research
{% comment %} This is now invalid as of 2023 -- Need to re-host on our drive {% endcomment %}
[sherlock-desc-PDF]: https://drive.google.com/file/d/0B_A1qX1kf7R9Q0llRWpkY2pXdzg/view

[nsf-award-1829771]: https://www.nsf.gov/awardsearch/showAward?AWD_ID=1829771
