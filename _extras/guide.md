---
title: "Instructor Notes"
---


## Research Case Study

The Big Data, Machine Learning, and Neural Networks lessons use the sample
SherLock dataset as their underlying research case study.
An overview with pointers to relevant details is provided
elsewhere in this lesson module:
[Dataset: SherLock (Android Smartphone Security)](
    {{ page.root }}/{% link _extras/dataset-sherlock.md %}
).


## Key Topics

The following are essential topics that are covered in tge "Big Data" lesson module:

  * Introduction of Series and DataFrame as the essential container objects
    in pandas;
  * Features and capabilities of DataFrame as a spreadsheet-like object;
  * Basic building blocks in tabular data analysis using DataFrame:
      - data selection
      - column-wise operations
      - filtering
      - sorting
      - aggregation
      - groupby
      - combining tables
  * Data cleaning and data wrangling;
  * Exploratory data analysis with visualization.



{% include links.md %}
