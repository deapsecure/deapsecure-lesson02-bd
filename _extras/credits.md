---
title: "Acknowledgments & Credits"
---

This page lists the sources of images and resources
from external sources that are used in this lesson.
Specifically, we strive to clearly indicate
copyrighted images and resources that are not licensed
in the same way as the rest of the lesson materials.
While we made our best efforts to obtain permission to use them in our materials,
please carefully follow the author's license terms (including the need to obtain permission
or license from them) if you plan to reuse these copyrighted resources
for your own instructional materials or projects.

Should you identify materials in this lesson that originated elsewhere
and were not credited properly,
please contact the
[authors](https://gitlab.com/deapsecure/deapsecure-lesson02-bd/-/blob/gh-pages/AUTHORS)
of this lesson so that they can promptly
provide the appropriate credit.


## Image Credits

* "Sherlock vs Moriarty"

  <https://commons.wikimedia.org/wiki/File:Gillette-Sherlock-Holmes-LIFE.jpg>

  Original source: Time Inc.; photograph by Byron Company, New York (1899).
  This work is in the public domain.

* "Sherlock vs Moriarty app"

  <http://bigdata.ise.bgu.ac.il/sherlock/assets/img/main_1.png>

  Copyright (c) 2016 Sherlock Team. All Rights Reserved. Used with permisison.

* "Sherlock data table stats August 2016 snapshot"

  <http://bigdata.ise.bgu.ac.il/sherlock/assets/img/dataset_5.png>

  Copyright (c) 2016 Sherlock Team. All Rights Reserved. Used with permisison.

* "Panel of various scatter plots to visualize the correlation of data."

  <https://en.wikipedia.org/wiki/File:Correlation_examples2.svg>

  Original source: 
  [Wikipedia; Correlation and Dependence](https://en.wikipedia.org/wiki/Correlation_and_dependence)
  This image was released into the public domain by the author DenisBoigelot.

* "About Jupyter Notebook"
 
  <https://docs.jupyter.org/en/latest/projects/architecture/content-architecture.html>

  Original source:
  [Jupyter Project Repository](https://github.com/jupyter/jupyter).
  Licensed under the BSD 3-Clause License.

<!--

* "**Series** - A one dimensional labeled array."

  <https://deapsecure.gitlab.io/deapsecure-lesson02-bd/fig/Series.png>

* "**DataFrame** - A twp-dimensional labeled data structure with multiple columns and 
  pissibly different data types"

  <https://deapsecure.gitlab.io/deapsecure-lesson02-bd/fig/DataFrame.png>

  -->



## Dataset Credits

### Sherlock Dataset

Original paper:
Yisroel Mirsky, Asaf Shabtia, Lior Rokach, Bracha Shapira, and
Yuval Elovici,
["SherLock vs Moriarty: A Smartphone Dataset for Cybersecurity Research"][sherlock-pub-2016],
9th ACM Workshop on Artificial Intelligence and Security (AISec)
with the 23nd ACM Conference on Computer and Communications (CCS), 2016.

Original project website: <http://bigdata.ise.bgu.ac.il/sherlock/index.html#/> .
(The website has been down since 2021 or so.
An [archived version of the website][sherlock-inet-archive] (retrieved 2019-08-29)
can be viewed on the Internet Archive.

We include a [detailed notes on SherLock dataset]({% link _extras/dataset-sherlock.md %})
on a separate page, detailing additional information such as:
(1) where to find the sample dataset,
(2) notes concerning the reduced datasets used in the DeapSECURE lessons.

The DeapSECURE team gratefully thanks Dr. Mirsky and his research team
for making a small sample dataset available publicly,
so that we can use this sample data in our lesson.



## Funding

The development of DeapSECURE is supported by the U.S. National Science Foundation (NSF)
through the [OAC/CyberTraining grant #1829771][nsf-award-1829771].


{% include links.md %}
