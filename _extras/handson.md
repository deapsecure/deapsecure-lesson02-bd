---
title: "Hands-on Materials"
---

## General Instructions

To download the notebooks and the hands-on files, please right-click on the links below and
select "Save Link As..." or a similar menu.
The notebooks are made available separately as a convenient way
to inspect and/or review them on the web.

The hands-on files are packed in a Zip format.
The main hands-on zip file also contains the notebooks.
Additional zip files may exist which provide additional
large data files for the hands-on activities.
To reconstitute: Unzip all the files into the same destination directory,
preserving the paths.

> ## Important Note
> Unless absolutely necessary, please download and use the newest version of the materials!
{: .prereq}


## Workshop Series 2020-2021 (Current)

### Resources: Jupyter Notebooks

- [Session 1: Fundamentals of Pandas][bd-notebook-ws2020-1] -
  ([html][bd-notebook-ws2020-1-html])
- [Session 2: Analytics of Sherlock Data with Pandas][bd-notebook-ws2020-2] -
  ([html][bd-notebook-ws2020-2-html])
- [Session 3: Data Wrangling and Visualization][bd-notebook-ws2020-3] -
  ([html][bd-notebook-ws2020-3-html])

(The HTML files were provided for convenient web viewing.)

### Resources: Hands-on Package

- [Sherlock hands-on files, except the large files][BD-sherlock]
  ([table of contents][BD-sherlock.contents])
- [Sherlock large data file: "sherlock_mystery_2apps.csv"][BD-sherlock-large-2apps]
  ([table of contents][BD-sherlock-large-2apps.contents])
- [Spam-ip based hands-on (legacy, optional)][BD-spam-ip]
  ([table of contents][BD-spam-ip.contents])

The hands-on files are packed in ZIP format.
The first two ZIP files above are mandatory.



## Pilot Workshop 2020 (Archived)

### Resources: Jupyter Notebooks

- [Session 1: Fundamentals of Pandas][bd-notebook-2020-1] -
  ([html][bd-notebook-2020-1-html])
- [Session 2: Analytics of Sherlock Data with Pandas][bd-notebook-2020-2] -
  ([html][bd-notebook-2020-2-html])
- [Session 3: Data Wrangling and Visualization][bd-notebook-2020-3] -
  ([html][bd-notebook-2020-3-html])

(The HTML files were provided for convenient web viewing.)



<!-- web links -->

[bd-notebook-ws2020-1]:      {{ page.root }}{% link files/ws-2020/BigData-session-1.ipynb %}
[bd-notebook-ws2020-2]:      {{ page.root }}{% link files/ws-2020/BigData-session-2.ipynb %}
[bd-notebook-ws2020-3]:      {{ page.root }}{% link files/ws-2020/BigData-session-3.ipynb %}
[bd-notebook-ws2020-1-html]: {{ page.root }}{% link files/ws-2020/BigData-session-1.html %}
[bd-notebook-ws2020-2-html]: {{ page.root }}{% link files/ws-2020/BigData-session-2.html %}
[bd-notebook-ws2020-3-html]: {{ page.root }}{% link files/ws-2020/BigData-session-3.html %}

[bd-notebook-2020-1]: {{ page.root }}{% link files/pilot-2020/BigData-session-1.ipynb %}
[bd-notebook-2020-2]: {{ page.root }}{% link files/pilot-2020/BigData-session-2.ipynb %}
[bd-notebook-2020-3]: {{ page.root }}{% link files/pilot-2020/BigData-session-3.ipynb %}
[bd-notebook-2020-1-html]: {{ page.root }}{% link files/pilot-2020/BigData-session-1.html %}
[bd-notebook-2020-2-html]: {{ page.root }}{% link files/pilot-2020/BigData-session-2.html %}
[bd-notebook-2020-3-html]: {{ page.root }}{% link files/pilot-2020/BigData-session-3.html %}

[BD-sherlock]:             {{ page.root }}{% link files/BD-sherlock.zip %}
[BD-sherlock-large-2apps]: {{ page.root }}{% link files/BD-sherlock-large-2apps.zip %}
[BD-spam-ip]:              {{ page.root }}{% link files/BD-spam-ip.zip %}
[BD-sherlock.contents]:    {{ page.root }}{% link files/BD-sherlock.contents %}
[BD-sherlock-large-2apps.contents]: {{ page.root }}{% link files/BD-sherlock-large-2apps.contents %}
[BD-spam-ip.contents]:     {{ page.root }}{% link files/BD-spam-ip.contents %}
